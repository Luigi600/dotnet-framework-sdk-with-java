FROM mcr.microsoft.com/dotnet/framework/sdk:4.8

RUN powershell (new-object System.Net.WebClient).Downloadfile('https://javadl.oracle.com/webapps/download/AutoDL?BundleId=244584_d7fc238d0cbf4b0dac67be84580cfb4b', 'C:\jre-8u291-windows-x64.exe')
RUN powershell start-process -filepath C:\jre-8u291-windows-x64.exe -passthru -wait -argumentlist "/s,INSTALLDIR=c:\Java\jre-8u291,/L,install64.log"
RUN del C:\jre-8u291-windows-x64.exe

ENV JAVA_HOME=c:\\Java\\jre-8u291
